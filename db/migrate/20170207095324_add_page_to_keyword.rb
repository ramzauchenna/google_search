class AddPageToKeyword < ActiveRecord::Migration[5.0]
  def change
    add_column :keywords, :page, :text
  end
end
