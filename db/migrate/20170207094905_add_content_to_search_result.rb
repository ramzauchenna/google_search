class AddContentToSearchResult < ActiveRecord::Migration[5.0]
  def change
    add_column :search_results, :content, :text
  end
end
