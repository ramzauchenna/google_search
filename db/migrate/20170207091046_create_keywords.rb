class CreateKeywords < ActiveRecord::Migration[5.0]
  def change
    create_table :keywords, force: true do |t|
      t.string :name
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
    add_index :keywords, [:name, :user_id]
  end
end
