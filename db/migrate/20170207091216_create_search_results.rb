class CreateSearchResults < ActiveRecord::Migration[5.0]
  def change
    create_table :search_results, force: true  do |t|
      t.belongs_to :keyword, foreign_key: true
      t.string :url
      t.string :title
      t.boolean :is_top_add
      t.boolean :is_bottom_add
      t.timestamps
    end
  end
end
