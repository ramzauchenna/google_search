json.search_results @search_results do |search_result|
  json.keyword search_result.keyword.name
  json.url search_result.clean_link
end
