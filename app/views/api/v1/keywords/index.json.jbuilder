json.keywords @keywords do |keyword|
  json.keyword keyword.name
  json.id keyword.id
end
