json.extract! @keyword, :id, :name, :description
json.keyword @keyword.name

json.search_results @keyword.search_results do |search_result|
  json.keyword search_result.keyword.name
  json.url search_result.clean_link
end