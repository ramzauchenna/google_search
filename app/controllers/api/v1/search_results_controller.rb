class Api::V1::SearchResultsController < Api::V1::BaseController
  def search_keywords
    @query = params[:keyword]
    @keywords = @user.search_results.where("url like :search or content like :search or keywords.name like :search", search: "%#{params[:@query]}%")
  end
end
