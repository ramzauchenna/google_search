class Api::V1::KeywordsController < Api::V1::BaseController

  def index
    @keywords = @user.keywords
  end

  def show
    @keywords = Keyword.find(params[:id])
  end


  def upload_keywords
    @keyword = Keyword.import(params[:file], @user)
    if @keyword
      render status: 200, json: {:status=>"success"}
    else
      respond_to do |format|
        format.json { render json: @keyword.errors, status: :unprocessable_entity }
      end
    end
  end
end
