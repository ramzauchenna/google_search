class Api::V1::BaseController < ApplicationController
  before_action :get_user


  private

  def get_user
    auth_token = params[:auth_token]
    if auth_token.nil?
      render status: 404, json: {:status=>"failed", message: "Cannot find your account."}
      return
    end

    @user = User.find_by(authentication_token: auth_token)
    if @user.nil?
      render status: 404, json: {status: "failed", message: "Cannot find your account. You must be a hacker"}
      return
    end
  end

end

