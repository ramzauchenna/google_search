class Api::V1::TokensController < ApplicationController
    def sign_in
      email = params[:email]
      password = params[:password]

      if email.nil? or password.nil?
        render status: 400,
               json: {status: "failed", message: "The request must contain the user phone number and password."}
        return
      end

      @user = User.find_by(email: email)

      if @user.nil?
        render status: 404, json: {status: "failed", message: "Invalid email or password."}
        return
      end


      if  !@user.valid_password?(password)
        render status: 400, json: {status: "failed", message: "Invalid email or password."}
      else
        respond_to do |format|
          format.json
        end
      end
    end

    def sign_up
      email = params[:email]
      password = params[:password]
      @user = User.new
      @user.email = email
      @user.password = password
      if @user.save
        render status: 200, json: {status: "success",
                                     token: @user.authentication_token, email: @user.email
                           }
      else
        respond_to do |format|
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

    def forgot_password
      email = params[:email]

      if email.nil?
        render status: 400,
               json: {status: "failed", message: "Please enter a valid email address."}
        return
      end

      @user = User.find_by(email: email)

      if @user.nil?
        render status: 404, json: {status: "failed", message: "Invalid email."}
        return
      else
        @user.send_reset_password_instructions
        respond_to do |format|
          format.json
        end
      end


    end



end

