class KeywordsController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :authenticate_user!
  def index
    @keywords = current_user.keywords.order("#{sort_column} #{sort_direction}").paginate(:page => params[:page], :per_page => 200)
  end

  def create
  end

  def show
    @keyword = Keyword.find(params[:id])
    @search_results = @keyword.search_results.order('id DESC')
    @top_ads = @search_results.top_ads
    @bottom_ads = @search_results.bottom_ads
    @search_query = @search_results.search_query
  end

  def search
    @q = params[:q]
    @search_results = current_user.search_results.where("url like :search or content like :search or keywords.name like :search", search: "%#{@q}%")
  end

  def search_keywords
    @query = params[:keyword]
    @keywords = @user.search_results.join(:keyword)
                    .where("url like :search or content like :search or keywords.name like :search", search: "%#{params[:@query]}%")
  end


  def import
    Keyword.import(params[:file], current_user)
    redirect_to keywords_url, notice: "keywords imported"
  end

  private
  def sortable_columns
    ["name", "price"]
  end

  def sort_column
    sortable_columns.include?(params[:column]) ? params[:column] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
