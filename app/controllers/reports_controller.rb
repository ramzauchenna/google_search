class ReportsController < ApplicationController
  before_action :authenticate_user!
  helper_method :sort_column, :sort_direction
  def index
    @search_results = SearchResult.joins(:keyword).order("#{sort_column} #{sort_direction}").paginate(:page => params[:page], :per_page => 200)
  end

  private
  def sortable_columns
    ["name", "price"]
  end

  def sort_column
    sortable_columns.include?(params[:column]) ? params[:column] : "keywords.name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
