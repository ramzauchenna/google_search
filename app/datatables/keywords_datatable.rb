class KeywordsDatatable
    delegate :params, :h, :link_to, to: :@view

    def initialize(view)
      @view = view
    end

    def as_json(options = {})
      {
          sEcho: params[:sEcho].to_i,
          iTotalRecords: SearchResult.count,
          iTotalDisplayRecords: search_results.size,
          aaData: data
      }
    end

    private

    def data
      search_results.map do |res|
        [
            res.keyword.name,
            res.url
        ]
      end
    end

    def search_results
      @search_results ||= fetch_keywords
    end

    def fetch_keywords
       search_results = SearchResult.order("#{sort_column} #{sort_direction}")
       search_results = search_results.page(page).per_page(per_page)
      if params[:sSearch].present?
        search_results = search_results.includes(:keyword).where("url like :search or content like :search or keywords.name like :search", search: "%#{params[:sSearch]}%")
      end
       search_results
    end

    def page
      params[:iDisplayStart].to_i/per_page + 1
    end

    def per_page
      params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
    end

    def sort_column
      puts params
      columns = %w[keyword_id url]
      columns[params[:iSortCol_0].to_i]
    end

    def sort_direction
      params[:sSortDir_0] == "desc" ? "desc" : "asc"
    end

end