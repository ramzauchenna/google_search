class SearchResult < ApplicationRecord
  belongs_to :keyword
  scope :top_ads, -> {where(is_top_add: true)}
  scope :bottom_ads, -> {where(is_bottom_add: true)}
  scope :search_query, -> {where.not(is_top_add: true).or(where.not(is_bottom_add: true))}

  def clean_link
    if url
      url.split('&').first
    end
  end
end
