class Keyword < ApplicationRecord
  belongs_to :user
  has_many :search_results


  def number_ads
    search_results.where(is_bottom_add: true).or(search_results.where(is_top_add: true)).size
  end




  def self.import(file, user)
    if File.extname(file.original_filename)
      CSV.foreach(file.path, headers: true) do |row|
        puts row
        GoogleScreenScrape.search(row["keyword"], user: user)
      end
    else
       raise "Unknown file type: #{file.original_filename}"
    end
  end
end
