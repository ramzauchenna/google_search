Rails.application.routes.draw do

  get 'reports/index'

  resources :keywords
  devise_for :users
  root 'welcome#index'
  get '/search' => "keywords#search", as: :search
  get '/reports' => "reports#index", as: :reports
  post '/upload-Keywords' => "keywords#import", as: :import
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  namespace :api do
    namespace :v1 do
      post 'sign_in' => "tokens#sign_in", as: :sign_in
      post 'sign_up' => "tokens#sign_up", as: :sign_up
      post 'forgot_password' => "tokens#forgot_password", as: :forgot_password
      get 'search' => "search_results#search", as: :search_api
      get 'upload_csv' => "search_results#search", as: :upload_csv
      get 'keywords' => "keywords#index", as: :keywords
      get 'keyword_details' => "keywords#show", as: :keyword_detail
    end
  end
end
