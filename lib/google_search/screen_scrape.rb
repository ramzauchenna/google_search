require 'nokogiri'
require 'open-uri'
require "uri"

module GoogleScreenScrape
  extend self

  def search(query, opts = {})
    # Get a Nokogiri::HTML:Document for the page we’re interested in...

    return nil unless query
    doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{query}"))
    entries = save_entries(doc, query)
    new_keyword = Keyword.new(name: query, page: doc, user: opts[:user])
    if new_keyword.save
      entries.each{ |e| new_keyword.search_results << e }
    end
    new_keyword
  end

  def save_entries(doc, query)
    entries = []


    content_block = doc.css('div#ires')[0]

    if content_block
      content_block.css('div.g').each do |link|
        result = SearchResult.new
        result.title = link.css("h3")[0].content
        result.url = link.css("h3 a")[0] ? URI.extract(link.css("h3 a")[0]["href"])[0] : nil
        result.content = link.css("span.st")[0] ? link.css("span.st")[0].content : nil
        result.is_top_add = false
        result.is_bottom_add = false
        if result.url
          entries.push(result)
        end
      end
    end


    top_ads_block  = doc.css('div#tvcap')[0]
    if top_ads_block
      top_ads_block.css('li.ads-ad').each do |link|
        result = SearchResult.new
        result.title = link.css("h3")[0].content
        result.url = URI.extract(link.css("h3 a")[1]["href"])[0]
        result.content = link.css("div.ads-creative")[0] ? link.css("div.ads-creative")[0] .content : nil
        result.is_top_add = true
        result.is_bottom_add = false
        if result.url
          entries.push(result)
        end
      end
    else
      doc.css('li.ads-ad').each do |link|
        result = SearchResult.new
        result.title = link.css("h3")[0].content
        result.url = link.css("cite")[0] ? link.css("cite")[0].content : nil
        result.content = link.css("div.ads-creative")[0] ? link.css("div.ads-creative")[0] .content : nil
        result.is_top_add = true
        result.is_bottom_add = false
        if result.url
          entries.push(result)
        end
      end
    end


    bottom_ads_block = doc.css('div#bottomads')[0]

    if bottom_ads_block
      bottom_ads_block.css('li.ads-ad').each do |link|
        result = SearchResult.new
        result.title = link.css("h3")[0].content
        result.url = URI.extract(link.css("h3 a")[1]["href"])[0]
        result.content = link.css("div.ads-creative")[0] ? link.css("div.ads-creative")[0] .content : nil
        result.is_top_add = false
        result.is_bottom_add = true
        if result.url
          entries.push(result)
        end
      end
    end
    entries
  end
end